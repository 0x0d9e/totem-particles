package me.divine.totemcolor.mixin;

import net.minecraft.client.particle.SpriteBillboardParticle;
import net.minecraft.client.particle.TotemParticle;
import net.minecraft.client.world.ClientWorld;

import org.spongepowered.asm.mixin.Mixin;
import org.spongepowered.asm.mixin.injection.At;
import org.spongepowered.asm.mixin.injection.Inject;
import org.spongepowered.asm.mixin.injection.callback.CallbackInfo;

import me.divine.totemcolor.TotemColorConfig;

@Mixin(TotemParticle.class)
public abstract class TotemParticleMixin extends SpriteBillboardParticle {
	protected TotemParticleMixin(ClientWorld clientWorld, double d, double e, double f) {
        super(null, 0.0, 0.0, 0.0);	
			
	}
	

	String r = ("0." + TotemColorConfig.COLOR_R + "F");
	String g = ("0." + TotemColorConfig.COLOR_G + "F");
	String b = ("0." + TotemColorConfig.COLOR_B + "F");

	float R = Float.parseFloat(r);  
	float G = Float.parseFloat(g);  
	float B = Float.parseFloat(b);  

	@Inject(at = @At("RETURN"), method = "<init>")
	private void onConstruct(CallbackInfo info) {
		if (this.random.nextInt(4) == 0) {
			this.setColor((float) 0.0F, (float) 0.191F, (float) 0.255F);
		 } else {
			this.setColor((float)0.255F, (float)0.255F, (float)0.255F);
		 }
	}  
}
