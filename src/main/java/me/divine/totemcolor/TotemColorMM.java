package me.divine.totemcolor;

import com.terraformersmc.modmenu.api.ConfigScreenFactory;
import com.terraformersmc.modmenu.api.ModMenuApi;

import me.shedaniel.autoconfig.AutoConfig;

public class TotemColorMM implements ModMenuApi {
    @Override
    public ConfigScreenFactory<?> getModConfigScreenFactory() {
        return (parent -> AutoConfig.getConfigScreen(TotemColorConfig.class, parent).get());
    }
}
