package me.divine.totemcolor;

import me.shedaniel.autoconfig.annotation.Config;
import me.shedaniel.autoconfig.ConfigData;

@Config(name = "alum")
public class TotemColorConfig implements ConfigData {
    public static float COLOR_R = 0;
    public static float COLOR_G = 255;
    public static float COLOR_B = 0;

}
